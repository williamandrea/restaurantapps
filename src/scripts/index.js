// import 'regenerator-runtime'; /* for async await transpile */
// import '../styles/main.css'
// import '../styles/main-responsive.css'
// import '../scripts/main.js'
// import '../scripts/handlebars.min.js'

document.addEventListener('DOMContentLoaded', () => {
  fetchData().then((res) => {
    renderTemplate(res)
  })
})

const renderTemplate = (data) => {
  const xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      const bodyContent = document.querySelector('#root')
      if (this.status == 200) {
        const content = xhttp.responseText
        const template = Handlebars.compile(content)
        const json = JSON.parse(data) 
        const Page = template(json.restaurants)
        bodyContent.innerHTML += Page
        handleDrawer()
      }
    }
  }
  xhttp.open('GET', `../templates/components/template.hbs`, true)
  xhttp.send()
}

const handleDrawer = () => {
  const hamburger = document.querySelector('.hamburger')
  const drawer = document.querySelector('#drawer')
  const body = document.querySelector('body')
  
  hamburger.addEventListener('click', (event) => {
    drawer.classList.toggle('open')
    event.stopPropagation()
  })

  body.addEventListener('click', (event) => {
    drawer.classList.remove('open')
    event.stopPropagation()
  })

  body.addEventListener('scroll', (event) => {
    drawer.classList.remove('open')
    event.stopPropagation()
  })

  body.addEventListener('wheel', (event) => {
    drawer.classList.remove('open')
    event.stopPropagation()
  })
}

const fetchData = () => {
  return new Promise((resolve) => {
    const xhttp = new XMLHttpRequest()
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4) {
        if (this.status == 200) {
          resolve(xhttp.responseText)
        }
      }
    }
    xhttp.open('GET', '/src/DATA.json', true)
    xhttp.send()
  })
} 

